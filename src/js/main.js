//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/foundation-sites/dist/foundation.min.js

//= partials/app.js

$(document).ready(function() {
    $(document).foundation();
 
    inputCheckValue();
    levelDetect();
});

function inputCheckValue(){
  
  $('form input').focusout(function(){
    var text_val = $(this).val();
      
    if(text_val === "") {

      $(this).removeClass('has-value');  

    } else {     

      $(this).addClass('has-value');  

    }
  });
}

function levelDetect(){
  
  var $checkboxes = $('.skills input[type="checkbox"]');
  
  $checkboxes.change(function(){
           
    var countCheckedCheckboxes = $checkboxes.filter(':checked').length;               
    var $angle = -30 + 14.1 * countCheckedCheckboxes;
    $('.level .meter .arrow').css('transform', 'rotate(' + $angle + 'deg)');
    
    $('.level p').text(countCheckedCheckboxes * 8);
    
    
  });
}